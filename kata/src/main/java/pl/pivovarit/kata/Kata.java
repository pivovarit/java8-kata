package pl.pivovarit.kata;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Test;

import pl.pivovarit.commons.dataset.Store;
import pl.pivovarit.commons.domain.Customer;
import pl.pivovarit.commons.domain.ItemEntry;
import pl.pivovarit.commons.domain.Shop;

public class Kata extends Store {

    private final List<Customer> customerList = getMall().getCustomerList();
    private final List<Shop> shopList = getMall().getShopList();

    /**
     * Find customers who have budget higher than 10_000
     */
    @Test
    public void P01_richCustomers() {

        // when
        final List<Customer> result = null; //TODO

        // then
        assertThat(result).hasSize(2);
        assertThat(result).contains(customerList.get(3), customerList.get(7));
    }

    /**
     * Find ages of all customers
     */
    @Test
    public void P02_howOldAreTheCustomers() {

        // when
        final List<Integer> result = null; //TODO

        // then
        assertThat(result).hasSize(10);
        assertThat(result).contains(22, 27, 28, 38, 26, 22, 32, 35, 21, 36);
    }

    /**
     * Create a list of customer's ascending ordered age values.
     */
    @Test
    public void P03_sortByAge() {

        // when
        final List<Integer> result = null; //TODO

        // then
        assertThat(result).contains(21, 22, 22, 26, 27, 28, 32, 35, 36, 38);
    }

    /**
     * Create a list of customer's descending ordered age values.
     */
    @Test
    public void P04_descSortByAge() {

        // when
        final List<Integer> result = null; //TODO

        // then
        assertThat(result).contains(38, 36, 35, 32, 28, 27, 26, 22, 22, 21);
    }

    /**
     * Find 3 richest customers
     */
    @Test
    public void P05_top3RichCustomer() {

        // when
        final List<String> result = null; //TODO

        // then
        assertThat(result).contains("Diana", "Andrew", "Chris");
    }

    /**
     * Find all customers age distinct values
     */
    @Test
    public void P06_distinctAge() {

        // when
        final List<Integer> result = null; //TODO

        // then
        assertThat(result).contains(22, 27, 28, 38, 26, 32, 35, 21, 36);
    }

    /**
     * Find all items customers want to buy
     */
    @Test
    public void P07_itemsCustomersWantToBuy() {
        // when
        final List<String> result = null; //TODO


        // then
        assertThat(result).contains("small table", "plate", "fork", "ice cream", "screwdriver", "cable", "earphone", "onion",
                "ice cream", "crisps", "chopsticks", "cable", "speaker", "headphone", "saw", "bond",
                "plane", "bag", "cold medicine", "chair", "desk", "pants", "coat", "cup", "plate", "fork",
                "spoon", "ointment", "poultice", "spinach", "ginseng", "onion");
    }

    /**
     * Find the name of the richest customer (willing to spend the most),
     * if no customers throw {@link NoSuchElementException}
     * *hint* reduce *hint*
     */
    @Test
    public void P08_richestCustomer() {
        // when
        final String result = null; //TODO

        // then
        assertThat(result).isEqualTo("Diana");
    }

    /**
     * Find what does the youngest customer want to buy.
     * If the youngest customer does not exist, return an empty list.
     */
    @Test
    public void P09_youngestCustomer() {

        // when
        final List<ItemEntry> result = null; //TODO

        // then
        assertThat(result)
                .extracting(ItemEntry::getName)
                .containsOnly("fork", "cup", "plate", "spoon");
    }

    /**
     * Find the first customer who registered this online store by using {@link Stream#findFirst}
     * Throw {@link NoSuchElementException} if such customer does not exist.
     * The customerList are ascending ordered by registered timing.
     */
    @Test
    public void P10_firstRegistrant() {

        // when
        final Customer result = null; //TODO

        // then
        assertThat(result).isEqualTo(customerList.get(0));
    }

    /**
     * Find whether any customer older than 40 exists or not.
     */
    @Test
    public void P11_isThereAnyoneOlderThan40() {
        // when
        Boolean result = null; //TODO

        // then
        assertThat(result).isFalse();
    }

    /**
     * Check whether all customer are older than 20 or not
     */
    @Test
    public void P12_isEverybodyOlderThan20() {
        // when
        Boolean result = null; //TODO

        // then
        assertThat(result).isTrue();
    }

    /**
     * Confirm that none of the customer has an empty WTB list
     */
    @Test
    public void P13_everyoneWantsSomething() {
        // when
        Boolean result = null; //TODO

        // then
        assertThat(result).isTrue();
    }

    /**
     * Create a list of customer names (no duplicates)
     */
    @Test
    public void P14_nameList() {
        // when
        final List<String> result = null; //TODO

        // then
        assertThat(result).containsOnly("Joe", "Steven", "Patrick", "Diana", "Chris", "Kathy", "Alice", "Andrew",
                "Martin", "Amy");
    }

    /**
     * Create a set of customer ages
     */
    @Test
    public void P15_ageSet() {
        // when
        final Set<Integer> result = null; //TODO

        // then
        assertThat(result).hasSize(9);
        assertThat(result).containsOnly(21, 22, 26, 27, 28, 32, 35, 36, 38);
    }

    /**
     * Create a csv string of customer names in brackets "[]"
     */
    @Test
    public void P16_nameInCsv() {
        // when
        final String result = null; //TODO

        // then
        assertThat(result).isEqualTo("[Joe,Steven,Patrick,Diana,Chris,Kathy,Alice,Andrew,Martin,Amy]");
    }

    /**
     * Create a map of age as key and number of customers as value
     */
    @Test
    public void P17_ageDistribution() {
        // when
        final Map<Integer, Long> result = null; //TODO

        // then
        assertThat(result).hasSize(9);
        result.forEach((k, v) -> {
            if (k.equals(22)) {
                assertThat(v).isEqualTo(2L);
            } else {
                assertThat(v).isEqualTo(1L);
            }
        });
    }

    /**
     * Calculate the average customer's age
     */
    @Test
    public void P18_averageAge() {
        // when
        final Double result = null; //TODO


        // then
        assertThat(result).isEqualTo(28.7);
    }

    /**
     * Calculate the sum of all items' prices
     */
    @Test
    public void P19_howMuchToBuyAllItems() {
        // when
        Long result = null; //TODO

        // then
        assertThat(result).isEqualTo(60930L);
    }

    /**
     * Create a set of item names that customers want to buy, but are not available anywhere
     */
    @Test
    public void P20_itemsNotOnSale() {

        // when
        final Set<String> result = null; //TODO

        // then
        assertThat(result).hasSize(3);
        assertThat(result).containsOnly("bag", "pants", "coat");
    }

    /**
     * Create a customer's name list including who can everything they want.
     * All items must be in stock.
     */
    @Test
    public void P21_havingEnoughMoney() {
        // when
        final List<String> customerNameList = null; //TODO

        // then
        assertThat(customerNameList).hasSize(8);
        assertThat(customerNameList).containsOnly("Joe", "Steven", "Patrick", "Diana", "Kathy", "Alice", "Martin", "Amy");
    }

}
