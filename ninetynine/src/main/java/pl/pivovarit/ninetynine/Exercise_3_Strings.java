package pl.pivovarit.ninetynine;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class Exercise_3_Strings {

    /**
     * Write a method that removes all spaces and capitalizes every "a" letter
     * @throws Exception
     */
    @Test
    public void StringOperations() throws Exception {
        // given
        final String input = "I never saw a purple cow";

        // when
        final String result = null; //TODO input.chars()


        // then
        Assertions.assertThat(result).isEqualTo("IneversAwApurplecow");
    }
}
