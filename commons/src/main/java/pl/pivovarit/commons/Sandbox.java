package pl.pivovarit.commons;

import pl.pivovarit.commons.optional.UserRepository;

public class Sandbox {
    public static final UserRepository repository = new UserRepository(false);

    public static void main(String[] args) {
    }
}
