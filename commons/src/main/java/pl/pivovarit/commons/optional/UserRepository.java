package pl.pivovarit.commons.optional;

import java.util.Optional;
import java.util.Random;

public class UserRepository {
    private final boolean randomize;

    public UserRepository(boolean randomize) {
        this.randomize = randomize;
    }

    public Optional<User> findByEmail(String email) {
        return email.equals("unknown") ? Optional.empty() : new Random()
                .ints().boxed().limit(1)
                .filter(i -> !randomize || i % 3 == 0)
                .findAny()
                .map(i -> new User(email, "Kraków"));
    }

}
