package pl.pivovarit.commons.optional;

import java.util.Optional;

public class User {
    private final String email;
    private final String address;

    public User(String email, String address) {
        this.email = email;
        this.address = address;
    }

    public User(String email) {
        System.out.println("Initializing: " + email + " with a null address");
        this.email = email;
        this.address = null;
    }

    public String getEmail() {
        return email;
    }

    public Optional<String> getAddress() {
        return Optional.ofNullable(address);
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
